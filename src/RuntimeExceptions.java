import java.util.ArrayList;
import java.util.List;

public class RuntimeExceptions {
	public static void main(String[] args) {

		try {
			int value = getArithmeticException(5, 0);
		} catch (ArithmeticException e) {
			System.out.println(e);
		}
		
		try {
			int value= getArrayIndexOutOfBoundsException(5, 1, 2, 3);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		}
		
		try {
			char c = getStringIndexOutOfBoundsException("Hello", 10);
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println(e);
		}
		
		try {
			int value = getNullPointerException(null);
		} catch (NullPointerException e) {
			System.out.println(e);
		}
		
		try {
			int value = getClassCastException("Hello");
		} catch (ClassCastException e) {
			System.out.println(e);
		}
		
		try {
			List<String> list = getIllegalArgumentException();
		} catch (IllegalArgumentException e) {
			System.out.println(e);
		}

	}

	private static List<String> getIllegalArgumentException() {
		return new ArrayList<String>(-5);
	}

	private static int getClassCastException(Object obj) {
		return (Integer)obj;
	}

	private static int getNullPointerException(int[] array) {
		return array.length;
	}

	private static char getStringIndexOutOfBoundsException(String string, int i) {
		return string.charAt(i);
	}

	private static int getArrayIndexOutOfBoundsException(int i, int... array) {
		return array[i];
	}

	private static int getArithmeticException(int num1, int num2) {
		return num1 / num2;
	}
}