public class ChainingException {
	public static void main(String[] args) {
		try {
			method1();
		} catch (Exception e) {
			System.out.println(e.getCause());
		}
	}

	private static void method1() {
		try {
			method2();
		} catch (Exception e) {
			ArithmeticException ae = new ArithmeticException();
			ae.initCause(e);
			throw ae;
		}
	}

	private static void method2() {
		throw new IllegalArgumentException();
	}
}
