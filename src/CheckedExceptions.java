import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;


public class CheckedExceptions {
	public static void main(String[] args) {
//		Scanner scanner = new Scanner(System.in);
//		String str = scanner.nextLine();
//		System.out.println(str);
//		
//		
//		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
//		try {
//			String str1 = br.readLine();
//		} catch (IOException e) {
//			System.out.println(e);
//		}
		
		File file = new File("D:\\123.txt");
		Scanner fileScanner = null;
		try {
			fileScanner = new Scanner(file);
		} catch (FileNotFoundException e) {
			System.out.println(e);
		}
		
		while (fileScanner.hasNext()) {
			System.out.println(fileScanner.nextLine());
		}
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
		} catch (FileNotFoundException e) {
			System.out.println(e);
		}
		
		try {
			while (br.ready()) {
				System.out.println(br.readLine());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		class MyClass implements Cloneable {
			@Override
			protected Object clone() throws CloneNotSupportedException {
				return super.clone();
			}
		}
		
		MyClass myClass = new MyClass();
		try {
			MyClass myClass2 = (MyClass)myClass.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println(e);
		}
	}
}
