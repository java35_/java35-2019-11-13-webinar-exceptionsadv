import java.io.Closeable;
import java.io.IOException;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class TryCatchResources {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
//		String str = scanner.nextLine();
//		System.out.println(str);
		
//		Before jdk 1.7
//		try {
//			scanner.nextInt();
//		} catch (Exception e) {
//		} finally {
//			scanner.close();
//		}
		
//		try (Scanner scanner1 = new Scanner(System.in)) {
//			int value = scanner1.nextInt();
//		} catch (Exception e) {
//			System.out.println(e);
//		}
//		
//		class MyClassCloseable implements Closeable {
//			@Override
//			public void close() throws IOException {
//				
//			}
//		}
//		
//		try (MyClassCloseable myClassCloseable = new MyClassCloseable()) {
//			
//		} catch (Exception e) {
////		}
//		
//		// Before jdk 1.7
//		int value;
//		try {
//			value = scanner.nextInt();
//		} catch (InputMismatchException ime) {
//			method1();
//		} catch (NoSuchElementException nse) {
//			method1();
//		} catch (IllegalStateException ise) {
//			method2();
//		}
//		
//		// After jdk 1.7
//		int value1 = 0;
//		try {
//			value1 = scanner.nextInt();
//		} catch (InputMismatchException | IllegalStateException e) {
//			method1();
//		} catch (NoSuchElementException ise) {
//			method2();
//		}
		
		try {
			int value = getIntFromConsole();
		} catch (NoSuchElementException | IllegalArgumentException e) {
			System.out.println(e);
		}
	}

	private static int getIntFromConsole() throws NoSuchElementException, IllegalArgumentException, InputMismatchException {
		try (Scanner scanner = new Scanner(System.in)) {
			return scanner.nextInt();
		} catch(Exception e) {
			throw e;
		}
	}

	private static void method2() {
		// TODO Auto-generated method stub
		
	}

	private static void method1() {
		// TODO Auto-generated method stub
		
	}
}
