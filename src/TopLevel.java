import java.util.function.Predicate;

public class TopLevel {
	static class StaticMemberClass {
		
	}
	
	class NonStaticClassMember {
		
	}
	
	void method() {
		class LocalClass {
			
		}
		
		LocalClass localClass = new LocalClass();
	}
	
	void method1() {
		Predicate<String> predicate = new Predicate<String>() {

			@Override
			public boolean test(String t) {
				return false;
			}
			
		};
	}
}
